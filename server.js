const request = require('request');
const path = require('path');
const history = require('connect-history-api-fallback');
const express = require('express');

const app = express();
const port = process.env.PORT || 3000;

function findUserByName(name, callback) {
  let userUrl = 'https://api-prod.andela.com/api/v1/users?role_ids=-KiihfZoseQeqC6bWTau&statuses=active';
  let url = userUrl + '&name=' + name;
  let headers = {
    'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySW5mbyI6eyJpZCI6Ii1LWEd5MU1VMW9pbWpRZ0ZpbURhIiwiZW1haWwiOiJicmlhbi5yb3RpY2hAYW5kZWxhLmNvbSIsImZpcnN0X25hbWUiOiJCcmlhbiIsImxhc3RfbmFtZSI6IlJvdGljaCIsIm5hbWUiOiJCcmlhbiBSb3RpY2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1BYmJxZnVadml3US9BQUFBQUFBQUFBSS9BQUFBQUFBQUFHSS9oM0x2RS1XWk5oZy9waG90by5qcGc_c3o9NTAiLCJyb2xlcyI6eyJBbmRlbGFuIjoiLUtpaWhmWm9zZVFlcUM2YldUYXUiLCJGZWxsb3ciOiItS1hHeTFFQjFvaW1qUWdGaW02QyJ9fSwiZXhwIjoxNTE1NTY2MzQ0fQ.h5_oC8FVYTEy2exBe2jSiZDb0XeXvEo_hUEcCb3FieLsPIJQRmnktqQKJV5ljN9NOokKYdZmo900wJ4MrSE0Syhu0rzf-wSYGXD8cuBcYIFgSNdiAYcE3tpO_JdOwM9m4I3GL812QOytpxycAzEneWxINmbMMW6yXlbBWWvo6C0'
  };

  request(url, {json: true, headers}, (err, res, body) => {
    if (err) {
      callback(err);
      return console.log(err);
    }
    if (res.statusCode !== 200) {
      console.error('unable to connected to andela');
      console.log('url', url);
      console.log('res', body);
      return callback(null, []);
    }
    let users = body.values || [];


    let results = users.map(user => {
      return {
        'id': user.id,
        'name': user.name,
        'email': user.email,
      }
    });
    callback(null, results);
  });
}



app.get('/users', (req, res) => {

  let query = req.query.q;

  findUserByName(query, (err, results) => {
    if (err) {
      res.status(500).json({
        'message': 'no results',
      });
    }

    res.json(results);
  });

});

let static = express.static('dist');
app.use(static);
// app.use(history({
//   disableDotRule: true,
//   verbose: true
// }));
// app.use(static);


app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
