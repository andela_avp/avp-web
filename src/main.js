// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue';
import App from './App';
import router from './router';

import { firebaseApp } from './firebase';

const VueFire = require('vuefire');

Vue.config.productionTip = false;

Vue.use(VueFire);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  created() {
    firebaseApp.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$router.push('/categories');
      } else {
        this.$router.push('/auth');
      }
    });
  },
});
