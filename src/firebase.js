const firebase = require('firebase');

const stagingConf = {
  apiKey: 'AIzaSyAUFX_mLi0XjrOWm4g2wjiAwt9sz2t7ZYg',
  authDomain: 'andela-avp-staging.firebaseapp.com',
  databaseURL: 'https://andela-avp-staging.firebaseio.com',
  projectId: 'andela-avp-staging',
  storageBucket: 'andela-avp-staging.appspot.com',
  messagingSenderId: '1055861035508',
};

const prodConf = {
  apiKey: 'AIzaSyCuZOLqfIvlN3i50JpYhgTxupjLJDwnUZg',
  authDomain: 'andela-avp-b2107.firebaseapp.com',
  databaseURL: 'https://andela-avp-b2107.firebaseio.com',
  projectId: 'andela-avp-b2107',
  storageBucket: 'andela-avp-b2107.appspot.com',
  messagingSenderId: '762569508132',
};
export const firebaseApp = firebase.initializeApp(stagingConf);
export const db = firebaseApp.database();
