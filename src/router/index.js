import Vue from 'vue';
import Router from 'vue-router';

import Auth from '@/components/auth';

import Landing from '@/components/Landing';
import Categories from '@/components/Categories';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/landing',
      name: 'Landing',
      component: Landing,
    },
    {
      path: '/categories',
      name: 'Categories',
      component: Categories,
    },
    {
      path: '/',
      component: Auth,
      name: 'Auth',
      alias: '/auth',
    },
  ],
});
